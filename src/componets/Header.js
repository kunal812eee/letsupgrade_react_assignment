import { React } from "react";
import '../App.css';

function Header(){
    return(
        <header className="appHeader">
            <h1 className="headerText">LetsUpgrade</h1>
        </header>
    );
}

export default Header