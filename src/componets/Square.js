import React from 'react'
import { propTypes } from 'react-bootstrap/esm/Image'

export default function Square(props) {
    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
    )
}