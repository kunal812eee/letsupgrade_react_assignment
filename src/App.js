import logo from './logo.svg';
import './App.css';
import Header from './componets/Header';
import Main from './componets/Main'
import StateExample from './componets/state/StateExample'
import Game from './componets/Game';

function App() {
  return (
    <div>
      <Header />
      <Main />
      <StateExample />
      <Game />
    </div>
  );
}

export default App;
