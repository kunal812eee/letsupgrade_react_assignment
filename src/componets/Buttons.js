import React from "react"
import { Button } from 'react-bootstrap'

function Buttons({name = false}){
    return(
        <div className='buttonsA'>
            <Button variant="outline-success">{name}</Button>{' '}
            <Button variant="outline-success">Success</Button>{' '}
            <Button variant="outline-warning">Warning</Button>{' '}
            <Button variant="outline-danger">Danger</Button>{' '}
        </div>
    );
}

export default Buttons
