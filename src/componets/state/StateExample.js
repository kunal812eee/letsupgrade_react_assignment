import {React, useState} from 'react'
import { Button } from 'react-bootstrap'

function StateExample() {
    let [score, setScore] = useState(0)

    return(
        <div>
            <h1>Assignment 2 </h1>
            <h3>Current Score {score}</h3>
            <Button variant="outline-success" onClick={() => score = score < 25 ? setScore(score+1) : false}>Increment</Button>{' '}
            <Button variant="outline-warning" onClick={() => score = score > 0 ? setScore(score-1) : ''}>Decrement</Button>{' '}
            <Button variant="outline-danger" onClick={() => setScore(0)}>Reset</Button>{' '}

        </div>
    );
}

export default StateExample